package com.its.assembly.config;

import com.its.assembly.properties.HelloProperties;
import com.its.assembly.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huguangjun
 * @className HelloServiceConfiguration
 * @date 2021/1/22
 * @desc 17:01
 */
@Configuration
@ConditionalOnWebApplication // web应用情况下才生效
@EnableConfigurationProperties(HelloProperties.class)
public class HelloServiceConfiguration {

    @Autowired
    private HelloProperties helloProperties;

    @Bean
    public HelloService helloService(){
        HelloService helloService = new HelloService();
        helloService.setHelloProperties(helloProperties);
        return helloService;
    }
}
