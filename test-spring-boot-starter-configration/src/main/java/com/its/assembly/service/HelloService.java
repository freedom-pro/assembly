package com.its.assembly.service;

import com.its.assembly.properties.HelloProperties;

/**
 * @author huguangjun
 * @className HelloService
 * @date 2021/1/22
 * @desc 16:53
 */
public class HelloService {

    private HelloProperties helloProperties;

    public HelloProperties getHelloProperties() {
        return helloProperties;
    }

    public void setHelloProperties(HelloProperties helloProperties) {
        this.helloProperties = helloProperties;
    }

    public String sayHello(String name) {
        return helloProperties.getPrefix() + "-" + name + "-" + helloProperties.getSuffix();
    }
}
