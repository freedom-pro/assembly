package com.its.assembly.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author huguangjun
 * @className HelloProperties
 * @date 2021/1/22
 * @desc 16:55
 */
// 绑定配置文件前缀所有以assembly.hello开始的
@ConfigurationProperties(prefix = "assembly.hello")
public class HelloProperties {

    private String prefix = "springBoot"; // 默认值
    private String suffix = "assembly"; // 默认值

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
